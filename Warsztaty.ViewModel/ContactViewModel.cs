﻿using System;

namespace Warsztaty.ViewModel
{
    public class ContactViewModel
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
